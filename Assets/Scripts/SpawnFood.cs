﻿using UnityEngine;
using System.Collections;

public class SpawnFood : MonoBehaviour {

    int layermask = 1 << 9;

    public enum FoodTypes { 
    
        Meat,
        Cheese,
        Onion,
        Tomatoe,
        Lettuce,
        Bread
    }

    public float foodSpawnDelay = 3f;

    public GameObject MeatPrefab;
    public GameObject CheesePrefab;
    public GameObject LettucePrefab;
    public GameObject OnionPrefab;
    public GameObject TomatoePrefab;
    public GameObject BreadPrefab;

    public FoodTypes myType;

    void Start() {
        CheckForFood();
    }
    void CheckForFood() {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down,out hit, Mathf.Infinity, layermask)){
            Debug.Log(hit.collider.gameObject.name);
            if(hit.collider.tag == "empty"){
                if(myType == FoodTypes.Meat){
                    Instantiate(MeatPrefab, transform.position, Quaternion.identity);
                }
                else if (myType == FoodTypes.Cheese)
                {
                    Instantiate(CheesePrefab, transform.position, Quaternion.identity);
                }
                else if (myType == FoodTypes.Onion)
                {
                    Instantiate(OnionPrefab, transform.position, Quaternion.identity);
                }
                else if (myType == FoodTypes.Lettuce)
                {
                    Instantiate(LettucePrefab, transform.position, Quaternion.identity);
                }
                else if (myType == FoodTypes.Tomatoe)
                {
                    Instantiate(TomatoePrefab, transform.position, Quaternion.identity);
                }
                else if(myType == FoodTypes.Bread){
                    Instantiate(BreadPrefab, transform.position, Quaternion.identity);
                }
            }
            StartCoroutine(CheckDelay());
        }
    
    }

    IEnumerator CheckDelay() {
        yield return new WaitForSeconds(foodSpawnDelay);
        CheckForFood();
    }
}
