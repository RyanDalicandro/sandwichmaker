﻿using UnityEngine;
using System.Collections;

public class MatchMousePos : MonoBehaviour {

    Vector3 mousePos;
    Ray mouseRay;
    int layermask = 1 << 8;

    public float power;

    Rigidbody rigid;

    //public float zpos;

	// Use this for initialization
	void Start () {
        rigid = gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        followMouse();
	}

    void followMouse() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layermask))
        {
            //mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Debug.Log("hit");
            
            //buggy movement
            //gameObject.transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);

            if(hit.point.x > transform.position.x){
                rigid.AddForce(new Vector3(1 * power,0,0));
            }
            else{
                rigid.AddForce(new Vector3(-1 * power, 0, 0));
            }

            if (hit.point.y > transform.position.y)
            {
                rigid.AddForce(new Vector3(0, 1 * power, 0));
            }
            else
            {
                rigid.AddForce(new Vector3(0, -1 * power, 0));
            }

            if (hit.point.z > transform.position.z)
            {
                rigid.AddForce(new Vector3(0, 0,1 * power));
            }
            else
            {
                rigid.AddForce(new Vector3(0, 0, -1 * power));
            }
        }


    }
}
