﻿using UnityEngine;
using System.Collections;

public class DestroyFood : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision col) { 
        if(col.gameObject.layer == 9){
            Destroy(col.gameObject);
        }
    }
}
