﻿using UnityEngine;
using System.Collections;

public class Grab : MonoBehaviour {

    Camera mainCam;

    public CamMan camMan;

    public GameObject DoneSign;

    public GameObject JointPrefab;
    int layermask = 1 << 9;

    GameObject activeJoint;
    HingeJoint joint;

	// Use this for initialization
	void Start () {
        mainCam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
        PlayerInput();
	}

    void PlayerInput() { 
        if(Input.GetKeyDown(KeyCode.Mouse0)){
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, Mathf.Infinity, layermask)){
                Debug.Log(hit.collider.gameObject.name);
                activeJoint = (GameObject)GameObject.Instantiate(JointPrefab, hit.point, Quaternion.identity);
                joint = activeJoint.GetComponent<HingeJoint>();
                if (hit.collider.gameObject.GetComponent<Rigidbody>() != null)
                {
                    joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody>();
                }

                if (hit.collider.tag == "done")
                {
                    Debug.Log("hitdone");
                    if(activeJoint != null){
                        Destroy(activeJoint);
                    }
                    DoneSign.SetActive(false);
                    camMan.EndGame();
                }


            }
        }

        if(Input.GetKeyUp(KeyCode.Mouse0)){
            Destroy(activeJoint);
        }
    }
}
