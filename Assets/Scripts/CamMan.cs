﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CamMan : MonoBehaviour {

    public Camera Glorycam;
    float glorytime = 5f;

    void Start() {
        Glorycam.enabled = false;
    }

    public void EndGame() {
        Camera.main.enabled = false;
        Glorycam.enabled = true;
        StartCoroutine(glorytimer());
    }

    IEnumerator glorytimer() {
        yield return new WaitForSeconds(glorytime);
        Debug.Log("game over");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
        
    }
}
